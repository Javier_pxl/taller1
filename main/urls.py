from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('team/', include('team.urls')),
    path('auth/', include('auth_login.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
