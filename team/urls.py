from django.urls import path
from django.conf import settings
from . import views

app_name = 'team'


urlpatterns = [
    path('', views.index, name='index'),                # /teams
    path('equipos/', views.teams, name='equipos'),         # /teams/teams
    path('entrenador/', views.coachs, name='entrenador'),     # /teams/coach
    path('jugador/', views.players, name='jugadores'),      # /teams/player
    path('jugador/add/', views.player_create, name='jadd'),
    path('equipos/add/', views.team_create, name='eadd'),
    path('entrenador/add/', views.coach_create, name='cadd'),
    #DELETE
    path('jugador/del/<int:pk>', views.player_delete, name='jdel'),
    path('equipos/del/<int:pk>', views.team_delete, name='edel'),
    path('entrenador/del/<int:pk>', views.coach_delete, name='cdel'),
    #UPDATE
    path('jugador/up/<int:pk>', views.player_upgrade, name='jup'),
    path('equipos/up/<int:pk>', views.team_upgrade, name='eup'),
    path('entrenador/up/<int:pk>', views.coach_upgrade, name='cup'),

    path('partido/', views.matchs, name='partido'),        # /teams/match

]

