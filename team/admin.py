from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User
from .models import Player,Team,Coach,Match


@admin.register(Player)
class playerAdmin(admin.ModelAdmin):
    search_fields = ['playerName', 'playerNickname', 'playerRut']    
    list_display = ('playerName','playerPositionGame','image_tag')    
    readonly_fields = ('image_tag',)

    radio_field ={"playerBirthdate": admin.HORIZONTAL}

@admin.register(Coach)
class coachAdmin(admin.ModelAdmin):
    pass
@admin.register(Team)
class teamAdmin(admin.ModelAdmin):
    list_display = ["teamName", "image_logo"]
    readonly_fields = ('image_logo',)


@admin.register(Match)
class matchAdmin(admin.ModelAdmin):
    pass
