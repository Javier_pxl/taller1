from django.shortcuts import render, HttpResponse,redirect, get_object_or_404
from django.template import loader
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators import login_required , permission_required
# from django.urls import reverse_lazy
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth import authenticate, login, get_user_model
from django.contrib import messages
from django.contrib.auth.models import Permission
from django import forms
from .models import *
from .forms import *


@login_required
def index(request):
    template = loader.get_template('home.html')
    print("user login", request.user)
    lastFiveMatches = Match.objects.order_by('id')[:3]
    lastFivePlayers = Player.objects.order_by('playerRut')[:3]
    lastThreeCoaches = Coach.objects.order_by('coachRut')[:3]
    allTeams =  Team.objects.all()
    context = {
        'lastFiveMatches': lastFiveMatches,
        'lastFivePlayers': lastFivePlayers,
        'allTeams': allTeams,
        'lastThreeCoaches' :lastThreeCoaches,
    }
    paginadorMatches = Paginator(lastFiveMatches,3)
    paginadorPlayers = Paginator(lastFivePlayers,3)
    paginadorTeams = Paginator(allTeams,3)

    page = request.GET.get('page',1)

    try:
        context['lastFiveMatches'] = paginadorMatches.page(page)
        context['lastFivePlayers'] = paginadorPlayers.page(page)
        context['allTeams'] = paginadorTeams.page(page)
    except PageNotAnInteger:
        context['lastFiveMatches'] = paginadorMatches.page(1)
        context['lastFivePlayers'] = paginadorPlayers.page(1)
        context['allTeams'] = paginadorTeams.page(1)
    except EmptyPage:
        context['lastFiveMatches'] = paginadorMatches.page(paginadorMatches.num_pages)
        context['lastFivePlayers'] = paginadorPlayers.page(paginadorPlayers.num_pages)
        context['allTeams'] = paginadorTeams.page(paginadorTeams.num_pages)
    
    return HttpResponse(template.render(context, request))


@login_required 
def teams(request):
    template = loader.get_template('teams.html')
    allTeams = Team.objects.all()
    allCoaches = Coach.objects.all()
    players = Player.objects.all()
    context = {
        'allTeam': allTeams,
        'allplayer' : players,
        'allCoaches': allCoaches,
    }

    paginator = Paginator(allTeams,6)
    page = request.GET.get('page',1)
    try:
        context['allTeam'] = paginator.page(page)
    except PageNotAnInteger:
        context['allTeam'] = paginator.page(1)
    except EmptyPage:
        context['allTeam'] = paginator.page(paginator.num_pages)

    return HttpResponse(template.render(context,request))

@permission_required('team.is_coach')
def team_create(request):
    template = 'team_create.html'
    context = {}
    context['form'] = TeamForm(request.POST or None,files=request.FILES or None)
    if context['form'].is_valid():
        context['form'].save()
        redirect('team:equipos')

    return render(request, template, context)

@login_required()
def team_delete(request, pk):
    template = 'team_delete.html'
    context = {}
    context['team'] = Team.objects.get(pk=pk)
    
    if request.method == 'POST':
        context['team'].delete()
        return redirect('team:equipos')
    return render(request, template, context)

@login_required()
def team_upgrade(request, pk):
    template= 'team_upgrade.html'
    context = {}

    team_create = get_object_or_404(Team, pk=pk)
    context['form'] = TeamForm(instance=team_create, data=request.POST)

    if context['form'].is_valid():
        context['form'].save()
        return redirect('team:equipos')

    else:
        print("error")

    return render(request, template, context)

@login_required()
def players(request):
    template = loader.get_template('players.html')
    allTeams = Team.objects.all() 
    allPlayers = Player.objects.all()   
    context = {
        'Teams': allTeams,
        'Players' : allPlayers,
      
    }
    paginator = Paginator(allPlayers,5)
    page = request.GET.get('page',1)
    try:
        context['Players'] = paginator.page(page)
    except PageNotAnInteger:
        context['Players'] = paginator.page(1)
    except EmptyPage:
        context['Players'] = paginator.page(paginator.num_pages)

    return HttpResponse(template.render(context,request))

@login_required()
def player_create(request):
    template = 'player_create.html'
    context = {}
    context['form'] = PlayerForm(request.POST or None,files=request.FILES or None)
    if context['form'].is_valid():
        context['form'].save()
        redirect('team:jugadores')
    else:
        pass   
    return render(request, template, context)

@login_required()
def player_delete(request, pk):
    template = 'player_delete.html'
    context = {}
    context['player'] = Player.objects.get(pk=pk)
    
    if request.method == 'POST':
        context['player'].delete()
        return redirect('player:jugadores')
    return render(request, template, context)

@login_required()
def player_upgrade(request, pk):
    template= 'player_upgrade.html'
    context = {}

    player = Player.objects.get(pk=pk)
    context['form'] = PlayerForm(request.POST or None, instance=player)

    if context['form'].is_valid():
        context['form'].save()
        return redirect('player.html')

    else:
        print("error")

    return render(request, template, context)

@login_required()
def coachs(request):
    template = loader.get_template('coach.html')
    allCoaches = Coach.objects.all()
    allTeams = Team.objects.all()
    context = {
        'Coaches' : allCoaches,
        'Teams' : allTeams,
    }
    paginator = Paginator(allCoaches,5)
    page = request.GET.get('page',1)
    try:
        context['Coaches'] = paginator.page(page)
    except PageNotAnInteger:
        context['Coaches'] = paginator.page(1)
    except EmptyPage:
        context['Coaches'] = paginator.page(paginator.num_pages)
    return HttpResponse(template.render(context, request))

@login_required()
def handle_uploaded_file(f):
    with open('some/file/name.txt', 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk) 
@login_required()
def coach_create(request):
    template = 'coach_create.html'

    if request.method == "POST":
        form = ExtendUserCreationForm(request.POST)
        coach_extend = UserCoachExtend(request.POST)

        if form.is_valid() and coach_extend.is_valid():
            user = form.save()
            
            content_type = ContentType.objects.get_for_model(Coach)
            permission = Permission.objects.get(
                    codename='is_coach',
                    content_type=content_type,
                )
            user.user_permissions.add(permission)
                
            coach_profile = coach_extend.save(commit=False)
            coach_profile.user = user
        
            coach_profile.save()

            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password1')
            user = authenticate(username=username,password=password)

            
            # login(request,user)

            messages.add_message(
                  request,
                  messages.SUCCESS,
                  'Usuario creado con éxito.'
                )

            return redirect('team:entrenador')
        else:
            messages.add_message(
              request,
              messages.ERROR,
              'Problemas al crear el usuario.'
            )            
    else:
        form = ExtendUserCreationForm()
        coach_extend = UserCoachExtend()
    
    context = {
        'form': form,
        'coach_extend': coach_extend,
    }
    return render(request, template, context)       

@login_required()
def coach_upgrade(request, pk):
    template = 'coach_upgrade.html'

    coach = get_object_or_404(Coach,pk=pk)
    if request.method == "POST":
        form = ExtendUserCreationForm(intance=coach, data=request.POST or None)
        coach_extend = UserCoachExtend(intance=coach, data=request.POST or None)
        if form.is_valid() and coach_extend.is_valid():
            user = form.save()
            coach_extend.save()
            messages.add_message(
                  request,
                  messages.SUCCESS,
                  'coach editado con éxito.'
                )
            return redirect('coach.html')
        else:
            messages.add_message(
              request,
              messages.ERROR,
              'Problemas al modificar el usuario.'
            )            
    else:
        form = ExtendUserCreationForm()
        coach_extend = UserCoachExtend()
    
    context = {
        'form': form,
        'coach_extend': coach_extend,
    }
    return render(request, template, context)       
@login_required()
def coach_delete(request, pk):
    template = 'coach_delete.html'
    context = {}
    context['coach'] = get_object_or_404(Coach, pk=pk)
    
    if request.method == 'POST':
        context['coach'].delete()
        return redirect('coach:jugadores')
    return render(request, template, context)

@login_required()
def matchs(request):
    template = loader.get_template('match.html')
    game = Match.objects.all()
    context = {
        'games':game

    }
    paginator = Paginator(game,3)
    page = request.GET.get('page',1)
    try:
        context['games'] = paginator.page(page)
    except PageNotAnInteger:
        context['games'] = paginator.page(1)
    except EmptyPage:
        context['games'] = paginator.page(paginator.num_pages)
    return HttpResponse(template.render(context, request))   


