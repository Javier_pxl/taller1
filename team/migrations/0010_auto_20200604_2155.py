# Generated by Django 3.0.6 on 2020-06-05 01:55

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('team', '0009_auto_20200604_2030'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='coach',
            options={'permissions': (('can_mark_returned', 'Set book as returned'),)},
        ),
    ]
