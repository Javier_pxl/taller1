from django.db import models
from django.conf import settings
from datetime import date
from django.utils.safestring import mark_safe
from django.contrib.auth.models import Permission
from django.contrib.auth.models import User


import os

class Coach(models.Model):
    user = models.OneToOneField(User,null=True, blank=True, on_delete=models.CASCADE)
    #extend
    # coachName = models.CharField('Name',max_length=100)
    coachAge = models.IntegerField('Age')
    # coachEmail = models.EmailField('Email',max_length=100)
    coachRut = models.CharField('Rut',primary_key = True, max_length=50,help_text=u"with point and dash")
    # coachNickname = models.TextField('nickname',null=True, blank=True)
    class Meta:
        permissions = (("is_coach", "coach"),)   

    def __str__(self):
        return str(self.user.username)



class Team(models.Model):
    teamName = models.CharField('Name',max_length=20)
    teamDescription = models.TextField('Description')
    teamLogo = models.ImageField('Image',upload_to='./logo/',height_field=None, width_field=None, max_length=None)
    def image_logo(self):
        return mark_safe('<img src="{}" width="100" />'.format(self.teamLogo.url))        
    image_logo.short_description = 'Imagimage_tage'
    image_logo.allow_tags = True 
    teamCode = models.AutoField(primary_key = True)
    coachRut = models.ForeignKey(Coach, on_delete=models.CASCADE,verbose_name="Coach")
    
    def __str__(self):
        return self.teamName

    class Meta:
        verbose_name = 'Team'
        verbose_name_plural = 'Teams'

class Player(models.Model):
    playerName = models.CharField('Name',max_length=50,help_text=u"Please enter your name...")
    playerNickname = models.CharField('NickName',null=True,blank=True,max_length=50)
    playerBirthdate = models.DateField('Birthdate',auto_now=False, auto_now_add=False,help_text=u"YYYY-MM-DD") 

    def get_year(self):
        return self.playerBirthdate.year    
    def set_age(self):
        edad = date.today().year - self.get_year()  
        playerAge = models.IntegerField(edad)

    playerRut = models.CharField('Rut',primary_key = True, max_length=50,help_text=u"with point and dash")
    playerEmail = models.EmailField('Email',null=True,blank=True, max_length=254)
    playerHeight = models.FloatField('Height',help_text=u"exm : 1.70")
    playerWeight = models.FloatField('weight',help_text=u"exm : 60")
    team = models.ForeignKey(Team, on_delete=models.CASCADE)
    playerImage = models.ImageField(upload_to='uploads')
    
    def image_tag(self):
        return mark_safe('<img src="{}" width="100" />'.format(self.playerImage.url))
    image_tag.short_description = 'Image'
    image_tag.allow_tags = True

    POSITION_CHOICES = [
    ('CE', 'Centre'),
    ('PF', 'Power Forward'),
    ('SF', 'Small Forward'),
    ('SG', 'Shooting Guard'),
    ('PG', 'Point Guard'),
]
    playerPositionGame = models.CharField('Position',max_length=60,choices = POSITION_CHOICES)

    def __str__(self):
        return self.playerName

    class Meta:
        verbose_name = 'Player'
        verbose_name_plural = 'Players'


class Match(models.Model):
    matchName = models.CharField(max_length=200)
    team1 = models.ForeignKey(Team,related_name='team1', on_delete=models.CASCADE)
    team2 = models.ForeignKey(Team,related_name='team12', on_delete=models.CASCADE)
    date = models.DateTimeField()

    class Meta:
        verbose_name = 'Match'
        verbose_name_plural = 'Matches'
    

    def __str__(self):
        return '{name} ({team1} vs {team2})'.format(
            name=self.matchName,
            team1=self.team1,
            team2=self.team2
        ) 




