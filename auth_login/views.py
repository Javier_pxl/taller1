from django.shortcuts import render, redirect
from django.contrib import auth

# Create your views here.


def login(request):
    template = 'login.html'
    context = {}
# °(o w o)°
    auth.logout(request)
    if request.POST:
        username = request.POST['username']
        password = request.POST['password']

        user = auth.authenticate(
            username=username,
            password=password
        )   
        if user is not None:
            if user.is_active:
                auth.login(request, user)
                return redirect('team:index') ## le mandas a la vista de team
            else: 
                #not valid
                print("error nomás no activo")
                
        else: #Not valid
            print("none")
        # print("es un POST!!")
        # print(request.POST['username'])
        

    return render(request,template,context)

def logout(request):
    auth.logout(request)
    return redirect('auth:login')